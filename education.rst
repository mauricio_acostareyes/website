Education
===============

.. figure:: university.logo.horizontal.blue.png
   :alt: university.logo.horizontal.blue.png
   :width: 70%

* Johns Hopkins University (2020-2022)
   * Master of Science - Data Science



.. figure:: FIU_hrz_Color.png
   :alt: FIU_hrz_Color.png
   :width: 50%

* Florida International University (2014-2019)
   * Bachelor of Science - Physics
   * Bachelor of Science - Biomedical Engineering
   * Minors - Mathematics, Mathematical Sciences