.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Mauricio Acosta
====================================================

.. toctree::
   :maxdepth: 1
   :caption: Welcome to my website!

   about_me.rst
   education.rst